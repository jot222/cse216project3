/**
 * <p>
 * Title: CSE216Crypto</p>
 *
 * <p>
 * Description: This class handles
 * the encrypt and decrypt operations for the
 * CSE216 Subsystem.
 * </p>
 *
 * <p>
 * Copyright: none</p>
 *
 * <p>
 * Company: Lehigh University</p>
 *
 * @author John Taulane
 *
 */
public class CSE216Crypto 
{
   private String input = " ";
   
   ///
   ///Encrypt works by xoring each character in the input string with 0x1
   ///Decrypt does the same thing to reverse the xor
   ///


   public void encrypt(String s){
      String result = "";
      char[] letters  = s.toCharArray();
      for(char c:letters){
        c = (char)(c^0x1);
        result += c;
     }
     input = result;
     System.out.println("Message succesfully encrpyted");
   } 
   public void decrypt(String s){
      String result = "";
      char[] letters = s.toCharArray();
      for(char c: letters){
         c = (char)(c^0x1);
         result += c;
         input = result;
      }
      input = result;
   }
   public String getMsg()    { return input; }
   
}
