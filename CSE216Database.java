/**
 * <p>
 * Title: CSE216Database</p>
 *
 * <p>
 * Description: This class stores messages
 * passed over the system.
 * </p>
 *
 * <p>
 * Copyright: none</p>
 *
 * <p>
 * Company: Lehigh University</p>
 *
 * @author John Taulane
 *
 */
import java.util.ArrayList;

public class CSE216Database{
    private ArrayList<String> messages = new ArrayList<String>();
    
    public void store(String s){
        messages.add(s);
    }

    public int getNumMsg(){
        return messages.size();
    }


}