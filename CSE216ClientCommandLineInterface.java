import java.util.*;


public class CSE216ClientCommandLineInterface {
   
   private final RunTimeVars rtv = 
                           RunTimeVars.Instance();
   private final ConfigFile cf   =  
                            ConfigFile.Instance(); 
   private final String hostname = 
                               cf.getMyHostName();
   private final int portnumber  = 
                              rtv.getServerPort();
   


   private final String prompt;
   
   private final Scanner    sc;
   
   private int             sel;
   
   public CSE216ClientCommandLineInterface
              (final String hname, final String m) 
   {

      prompt = "Host: " + hostname
              + "     Node ID: " + m + "\n"
              + "1) Enter 1 if you want your message to be case sensitive!\n"
              + "2) Enter 2 for your message to be defaulted to uppercase\n"
              + "Or enter anything else to quit\n";
      
      sc = new Scanner(System.in);
      
   }
   
   public CState getUserSelection() 
   {
      CState cs =  new CState();
      cs.mid    = MessageID.MSG;
      String myMessage = "temp";
      Scanner myScan = new Scanner(System.in);
      
      System.out.print (prompt);
      try {
             sel = sc.nextInt();
      } catch (Exception e) 
      {
         System.exit        (0);
      }
      
      if (sel == 1) 
      {
         cs.setV(1);
         System.out.println("Enter a string for your message");
         myMessage = myScan.nextLine();
         //Do something cool here
         cs.setMessage(myMessage);
      }
      else if (sel == 2)
      {
         cs.setV(2);
         System.out.println("Enter a string for your message");
         myMessage = myScan.nextLine().toUpperCase();
         cs.setMessage(myMessage);
      } 
      else 
         System.exit(0);

      
      return cs;
      
   }
   
}
